class CreateEmployees < ActiveRecord::Migration[6.1]
  def change
    create_table :employees do |t|
      t.references :company, null: false, foreign_key: true
      t.references :office, foreign_key: true
      t.string :number
      t.string :name, null: false

      t.timestamps
    end
  end
end
