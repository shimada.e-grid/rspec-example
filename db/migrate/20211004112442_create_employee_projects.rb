class CreateEmployeeProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :employee_projects do |t|
      t.references :employee, null: false, foreign_key: true
      t.references :project, null: false, foreign_key: true
      t.date :join_date
      t.date :leave_date

      t.timestamps
    end
  end
end
