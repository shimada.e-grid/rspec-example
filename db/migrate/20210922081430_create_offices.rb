class CreateOffices < ActiveRecord::Migration[6.1]
  def change
    create_table :offices do |t|
      t.references :company, null: false, foreign_key: true
      t.boolean :main_office_flag, null: false, default: false
      t.string :name, null: false
      t.string :address
      t.string :tel

      t.timestamps
    end
  end
end
