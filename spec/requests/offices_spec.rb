require 'rails_helper'

RSpec.describe "/offices", type: :request do
  let(:company) {
    FactoryBot.create(:company)
  }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:office)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:office, name: '')
  }

  describe "GET /index" do
    it 'レスポンスが正常であること' do
      company.offices.create! valid_attributes
      get company_offices_url(company)
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it 'レスポンスが正常であること' do
      office = company.offices.create! valid_attributes
      get company_office_url(company, office)
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it 'レスポンスが正常であること' do
      get new_company_office_url(company)
      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "レスポンスが正常であること" do
      office = company.offices.create! valid_attributes
      get edit_company_office_url(company, office)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "有効なパラメータの場合" do
      it "Officeが新規作成されること" do
        expect {
          post company_offices_url(company), params: { office: valid_attributes }
        }.to change(Office, :count).by(1)
      end

      it "作成されたOfficeの詳細ページへリダイレクトされること" do
        post company_offices_url(company), params: { office: valid_attributes }
        expect(response).to redirect_to(company_office_url(company, Office.last))
      end
    end

    context "無効なパラメータの場合" do
      it "Officeが新規作成されないこと" do
        expect {
          post company_offices_url(company), params: { office: invalid_attributes }
        }.to change(Office, :count).by(0)
      end

      it "レスポンスが正常であること (新規作成ページを描画)" do
        post company_offices_url(company), params: { office: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PATCH /update" do
    context "有効なパラメータの場合" do
      let(:new_attributes) {
        FactoryBot.attributes_for(:company, name: '本社２')
      }

      it "Officeが更新できること" do
        office = company.offices.create! valid_attributes
        patch company_office_url(company, office), params: { office: new_attributes }
        office.reload
        expect(office).to have_attributes new_attributes
      end

      it "更新されたOfficeの詳細ページへリダイレクトされること" do
        office = company.offices.create! valid_attributes
        patch company_office_url(company, office), params: { office: new_attributes }
        office.reload
        expect(response).to redirect_to(company_office_url(company, office))
      end
    end

    context "無効なパラメータの場合" do
      it "レスポンスが正常であること (編集ページを描画)" do
        office = company.offices.create! valid_attributes
        patch company_office_url(company, office), params: { office: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE /destroy" do
    it "Officeが削除されること" do
      office = company.offices.create! valid_attributes
      expect {
        delete company_office_url(company, office)
      }.to change(Office, :count).by(-1)
    end

    it "Office一覧へリダイレクトされること" do
      office = company.offices.create! valid_attributes
      delete company_office_url(company, office)
      expect(response).to redirect_to(company_offices_url)
    end
  end
end
