require 'rails_helper'

RSpec.describe '/companies', type: :request do
  # 正常系データ
  let(:valid_attributes) {
    FactoryBot.attributes_for(:company)
  }

  # 異常系データ
  let(:invalid_attributes) {
    FactoryBot.attributes_for(:company, name: '')
  }

  describe 'GET /index' do
    it 'レスポンスが正常であること' do
      # companyデータをDBへ保存
      Company.create! valid_attributes
      # company一覧へアクセス
      get companies_url
      # レスポンスが正常かテスト
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'レスポンスが正常であること' do
      company = Company.create! valid_attributes
      # company詳細へアクセス
      get company_url(company)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'レスポンスが正常であること' do
      # company新規作成へアクセス
      get new_company_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'レスポンスが正常であること' do
      company = Company.create! valid_attributes
      get edit_company_url(company)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context '有効なパラメータの場合' do
      it 'Companyが新規作成されること' do
        expect {
          post companies_url, params: { company: valid_attributes }
        }.to change(Company, :count).by(1)
      end

      it '作成されたCompanyの詳細ページへリダイレクトされること' do
        post companies_url, params: { company: valid_attributes }
        expect(response).to redirect_to(company_url(Company.last))
      end
    end

    context '無効なパラメータの場合' do
      it 'Companyが新規作成されないこと' do
        expect {
          post companies_url, params: { company: invalid_attributes }
        }.to change(Company, :count).by(0)
      end

      it 'レスポンスが正常であること (新規作成ページを描画)' do
        post companies_url, params: { company: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update' do
    context '有効なパラメータの場合' do
      let(:new_attributes) {
        FactoryBot.attributes_for(:company, name: '株式会社テスト２')
      }

      it 'Companyが更新できること' do
        company = Company.create! valid_attributes
        patch company_url(company), params: { company: new_attributes }
        company.reload
        expect(company.name).to eq new_attributes[:name]
      end

      it '更新されたCompanyの詳細ページへリダイレクトされること' do
        company = Company.create! valid_attributes
        patch company_url(company), params: { company: new_attributes }
        company.reload
        expect(response).to redirect_to(company_url(company))
      end
    end

    context '無効なパラメータの場合' do
      it 'レスポンスが正常であること (編集ページを描画)' do
        company = Company.create! valid_attributes
        patch company_url(company), params: { company: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'Companyが削除されること' do
      company = Company.create! valid_attributes
      expect {
        delete company_url(company)
      }.to change(Company, :count).by(-1)
    end

    it 'Company一覧へリダイレクトされること' do
      company = Company.create! valid_attributes
      delete company_url(company)
      expect(response).to redirect_to(companies_url)
    end
  end
end
