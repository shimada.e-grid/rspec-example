require 'rails_helper'

RSpec.describe "/projects", type: :request do
  let(:company) {
    FactoryBot.create(:company)
  }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:project, employee_projects_attributes: [])
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:project, name: '', employee_projects_attributes: [])
  }

  describe "GET /index" do
    it 'レスポンスが正常であること' do
      company.projects.create! valid_attributes
      get company_projects_url(company)
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it 'レスポンスが正常であること' do
      project = company.projects.create! valid_attributes
      get company_project_url(company, project)
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it 'レスポンスが正常であること' do
      get new_company_project_url(company)
      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "レスポンスが正常であること" do
      project = company.projects.create! valid_attributes
      get edit_company_project_url(company, project)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "有効なパラメータの場合" do
      it "Projectが新規作成されること" do
        expect {
          post company_projects_url(company), params: { project: valid_attributes }
        }.to change(Project, :count).by(1)
      end

      it "作成されたprojectの詳細ページへリダイレクトされること" do
        post company_projects_url(company), params: { project: valid_attributes }
        should redirect_to(company_project_url(company, Project.last))
      end
    end

    context "無効なパラメータの場合" do
      it "Projectが新規作成されないこと" do
        expect {
          post company_projects_url(company), params: { project: invalid_attributes }
        }.to change(Project, :count).by(0)
      end

      it "レスポンスが正常であること (新規作成ページを描画)" do
        post company_projects_url(company), params: { project: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PATCH /update" do
    context "有効なパラメータの場合" do
      let(:new_attributes) {
        FactoryBot.attributes_for(:company, name: '本社２')
      }

      it "Projectが更新できること" do
        project = company.projects.create! valid_attributes
        patch company_project_url(company, project), params: { project: new_attributes }
        project.reload
        expect(project).to have_attributes new_attributes
      end

      it "更新されたProjectの詳細ページへリダイレクトされること" do
        project = company.projects.create! valid_attributes
        patch company_project_url(company, project), params: { project: new_attributes }
        project.reload
        expect(response).to redirect_to(company_project_url(company, project))
      end
    end

    context "無効なパラメータの場合" do
      it "レスポンスが正常であること (編集ページを描画)" do
        project = company.projects.create! valid_attributes
        patch company_project_url(company, project), params: { project: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE /destroy" do
    it "Projectが削除されること" do
      project = company.projects.create! valid_attributes
      expect {
        delete company_project_url(company, project)
      }.to change(Project, :count).by(-1)
    end

    it "Project一覧へリダイレクトされること" do
      project = company.projects.create! valid_attributes
      delete company_project_url(company, project)
      # expect(response).to redirect_to(company_projects_url)
      should redirect_to(action: :index)
    end
  end
end
