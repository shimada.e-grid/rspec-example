require 'rails_helper'

RSpec.describe "/employees", type: :request do
  let(:company) {
    FactoryBot.create(:company)
  }

  let(:office) {
    FactoryBot.create(:office, :branch_office)
  }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:employee)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:employee, name: '')
  }

  describe "GET /index" do
    it 'レスポンスが正常であること' do
      company.employees.create! valid_attributes
      get company_employees_url(company)
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it 'レスポンスが正常であること' do
      employee = company.employees.create! valid_attributes
      get company_employee_url(company, employee)
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it 'レスポンスが正常であること' do
      get new_company_employee_url(company)
      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "レスポンスが正常であること" do
      employee = company.employees.create! valid_attributes
      get edit_company_employee_url(company, employee)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "有効なパラメータの場合" do
      it "Employeeが新規作成されること" do
        expect {
          post company_employees_url(company), params: { employee: valid_attributes }
        }.to change(Employee, :count).by(1)
      end

      it "作成されたEmployeeの詳細ページへリダイレクトされること" do
        post company_employees_url(company), params: { employee: valid_attributes }
        expect(response).to redirect_to(company_employee_url(company, Employee.last))
      end
    end

    context "無効なパラメータの場合" do
      it "Employeeが新規作成されないこと" do
        expect {
          post company_employees_url(company), params: { employee: invalid_attributes }
        }.to change(Employee, :count).by(0)
      end

      it "レスポンスが正常であること (新規作成ページを描画)" do
        post company_employees_url(company), params: { employee: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PATCH /update" do
    context "有効なパラメータの場合" do
      let(:new_attributes) {
        FactoryBot.attributes_for(:company, name: '本社２')
      }

      it "Employeeが更新できること" do
        employee = company.employees.create! valid_attributes
        patch company_employee_url(company, employee), params: { employee: new_attributes }
        employee.reload
        expect(employee).to have_attributes new_attributes
      end

      it "更新されたEmployeeの詳細ページへリダイレクトされること" do
        employee = company.employees.create! valid_attributes
        patch company_employee_url(company, employee), params: { employee: new_attributes }
        employee.reload
        expect(response).to redirect_to(company_employee_url(company, employee))
      end
    end

    context "無効なパラメータの場合" do
      it "レスポンスが正常であること (編集ページを描画)" do
        employee = company.employees.create! valid_attributes
        patch company_employee_url(company, employee), params: { employee: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE /destroy" do
    it "Employeeが削除されること" do
      employee = company.employees.create! valid_attributes
      expect {
        delete company_employee_url(company, employee)
      }.to change(Employee, :count).by(-1)
    end

    it "Employee一覧へリダイレクトされること" do
      employee = company.employees.create! valid_attributes
      delete company_employee_url(company, employee)
      expect(response).to redirect_to(company_employees_url)
    end
  end
end
