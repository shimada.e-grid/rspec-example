require "rails_helper"

RSpec.describe OfficesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/companies/1/offices").to route_to("offices#index", company_id: "1")
    end

    it "routes to #new" do
      expect(get: "/companies/1/offices/new").to route_to("offices#new", company_id: "1")
    end

    it "routes to #show" do
      expect(get: "/companies/1/offices/1").to route_to("offices#show", company_id: "1", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/companies/1/offices/1/edit").to route_to("offices#edit", company_id: "1", id: "1")
    end


    it "routes to #create" do
      expect(post: "/companies/1/offices").to route_to("offices#create", company_id: "1")
    end

    it "routes to #update via PUT" do
      expect(put: "/companies/1/offices/1").to route_to("offices#update", company_id: "1", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/companies/1/offices/1").to route_to("offices#update", company_id: "1", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/companies/1/offices/1").to route_to("offices#destroy", company_id: "1", id: "1")
    end
  end
end
