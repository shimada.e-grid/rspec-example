require "rails_helper"

# gemのshoulda-matchersを使用した場合
RSpec.describe ProjectsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      should route(:get, '/companies/1/projects').to(action: :index, company_id: 1)
    end

    it "routes to #new" do
      should route(:get, '/companies/1/projects/new').to(action: :new, company_id: 1)
    end

    it "routes to #show" do
      should route(:get, '/companies/1/projects/1').to(action: :show, company_id: 1, id: 1)
    end

    it "routes to #edit" do
      should route(:get, '/companies/1/projects/1/edit').to(action: :edit, company_id: 1, id: 1)
    end


    it "routes to #create" do
      should route(:post, '/companies/1/projects').to(action: :create, company_id: 1)
    end

    it "routes to #update via PUT" do
      should route(:put, '/companies/1/projects/1').to(action: :update, company_id: 1, id: 1)
    end

    it "routes to #update via PATCH" do
      should route(:patch, '/companies/1/projects/1').to(action: :update, company_id: 1, id: 1)
    end

    it "routes to #destroy" do
      should route(:delete, '/companies/1/projects/1').to(action: :destroy, company_id: 1, id: 1)
    end
  end
end
