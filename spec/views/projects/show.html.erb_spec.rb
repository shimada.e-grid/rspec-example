require 'rails_helper'

RSpec.describe "projects/show", type: :view do
  before(:each) do
    @project = assign(:project, FactoryBot.create(:project))
  end

  it "値が描画されていること" do
    render
    expect(rendered).to match(@project.company.name)
    expect(rendered).to match(@project.name)
  end
end
