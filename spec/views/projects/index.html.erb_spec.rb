require 'rails_helper'

RSpec.describe "projects/index", type: :view do
  before(:each) do
    @company = FactoryBot.create(:company)
    assign(:projects, FactoryBot.create_list(:project, 2, company: @company, name: 'テストプロジェクト２'))
  end

  it "project一覧が描画されること" do
    render
    assert_select "tr>td", text: @company.name.to_s, count: 2
    assert_select "tr>td", text: "テストプロジェクト２".to_s, count: 2
  end
end
