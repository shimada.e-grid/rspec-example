require 'rails_helper'

RSpec.describe "projects/edit", type: :view do
  before(:each) do
    @project = assign(:project, FactoryBot.create(:project))
  end

  it "project編集フォームが描画されること" do
    render

    assert_select "form[action=?][method=?]", company_project_path(@project.company, @project), "post" do

      expect(rendered).to match(@project.company.name)

      assert_select "input[name=?]", "project[name]"
    end
  end
end
