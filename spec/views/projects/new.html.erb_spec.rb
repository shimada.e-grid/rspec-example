require 'rails_helper'

RSpec.describe "projects/new", type: :view do
  before(:each) do
    @company = FactoryBot.create(:company)
    assign(:project, FactoryBot.build(:project, company: @company))
  end

  it "project新規作成フォームが描画されること" do
    render

    assert_select "form[action=?][method=?]", company_projects_path(@company), "post" do

      expect(rendered).to match(@company.name)

      assert_select "input[name=?]", "project[name]"
    end
  end
end
