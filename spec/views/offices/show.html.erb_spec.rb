require 'rails_helper'

RSpec.describe "offices/show", type: :view do
  before(:each) do
    @office = assign(:office, FactoryBot.create(:office))
  end

  it "値が描画されていること" do
    render
    expect(rendered).to match(@office.company.name)
    expect(rendered).to match(@office.main_office_flag.to_s)
    expect(rendered).to match(@office.name)
    expect(rendered).to match(@office.address)
    expect(rendered).to match(@office.tel)
  end
end
