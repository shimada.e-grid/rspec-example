require 'rails_helper'

RSpec.describe "offices/edit", type: :view do
  before(:each) do
    @office = assign(:office, FactoryBot.create(:office))
  end

  it "office編集フォームが描画されること" do
    render

    assert_select "form[action=?][method=?]", company_office_path(@office.company, @office), "post" do

      expect(rendered).to match(@office.company.name)

      assert_select "input[name=?]", "office[main_office_flag]"

      assert_select "input[name=?]", "office[name]"

      assert_select "input[name=?]", "office[address]"

      assert_select "input[name=?]", "office[tel]"
    end
  end
end
