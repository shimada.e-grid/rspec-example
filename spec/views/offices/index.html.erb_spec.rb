require 'rails_helper'

RSpec.describe "offices/index", type: :view do
  before(:each) do
    @company = FactoryBot.create(:company)
    assign(:offices, FactoryBot.create_list(:office, 2, company: @company, name: 'テスト', address: 'テストアドレス', tel: 'テストtel'))
  end

  it "office一覧が描画されること" do
    render
    assert_select "tr>td", text: @company.name.to_s, count: 2
    assert_select "tr>td", text: true.to_s, count: 2
    assert_select "tr>td", text: "テスト".to_s, count: 2
    assert_select "tr>td", text: "テストアドレス".to_s, count: 2
    assert_select "tr>td", text: "テストtel".to_s, count: 2
  end
end
