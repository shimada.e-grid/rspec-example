require 'rails_helper'

RSpec.describe "offices/new", type: :view do
  before(:each) do
    @company = FactoryBot.create(:company)
    assign(:office, FactoryBot.build(:office, company: @company))
  end

  it "office新規作成フォームが描画されること" do
    render

    assert_select "form[action=?][method=?]", company_offices_path(@company), "post" do

      expect(rendered).to match(@company.name)

      assert_select "input[name=?]", "office[main_office_flag]"

      assert_select "input[name=?]", "office[name]"

      assert_select "input[name=?]", "office[address]"

      assert_select "input[name=?]", "office[tel]"
    end
  end
end
