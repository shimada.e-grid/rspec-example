require 'rails_helper'

RSpec.describe "employees/index", type: :view do
  before(:each) do
    @company = FactoryBot.create(:company)
    @office = FactoryBot.create(:office)
    assign(:employees, FactoryBot.create_list(:employee, 2, company: @company, office: @office, number: '1010', name: 'テスト太郎'))
  end

  it "employee一覧が描画されること" do
    render
    assert_select "tr>td", text: @company.name.to_s, count: 2
    assert_select "tr>td", text: @office.name.to_s, count: 2
    assert_select "tr>td", text: "1010".to_s, count: 2
    assert_select "tr>td", text: "テスト太郎".to_s, count: 2
  end
end
