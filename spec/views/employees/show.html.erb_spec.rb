require 'rails_helper'

RSpec.describe "employees/show", type: :view do
  before(:each) do
    @employee = assign(:employee, FactoryBot.create(:employee))
  end

  it "値が描画されていること" do
    render
    expect(rendered).to match(@employee.company.name)
    expect(rendered).to match(@employee.office.name)
    expect(rendered).to match(@employee.number)
    expect(rendered).to match(@employee.name)
  end
end
