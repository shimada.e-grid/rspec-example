require 'rails_helper'

RSpec.describe "employees/new", type: :view do
  before(:each) do
    @company = FactoryBot.create(:company)
    @office = FactoryBot.create(:office)
    assign(:employee, FactoryBot.build(:employee, company: @company, office: @office))
  end

  it "employee新規作成フォームが描画されること" do
    render

    assert_select "form[action=?][method=?]", company_employees_path(@company), "post" do

      expect(rendered).to match(@company.name)

      expect(rendered).to match(@office.name)

      assert_select "input[name=?]", "employee[number]"

      assert_select "input[name=?]", "employee[name]"
    end
  end
end
