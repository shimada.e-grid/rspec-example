require 'rails_helper'

RSpec.describe "employees/edit", type: :view do
  before(:each) do
    @employee = assign(:employee, FactoryBot.create(:employee))
  end

  it "employee編集フォームが描画されること" do
    render

    assert_select "form[action=?][method=?]", company_employee_path(@employee.company, @employee), "post" do

      expect(rendered).to match(@employee.company.name)

      expect(rendered).to match(@employee.office.name)

      assert_select "input[name=?]", "employee[number]"

      assert_select "input[name=?]", "employee[name]"
    end
  end
end
