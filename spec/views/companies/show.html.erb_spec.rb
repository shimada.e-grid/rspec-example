require 'rails_helper'

RSpec.describe "companies/show", type: :view do
  before(:each) do
    @company = assign(:company, FactoryBot.create(:company, name: 'テスト'))
  end

  it "値が描画されていること" do
    render
    expect(rendered).to match(@company.name)
  end
end
