require 'rails_helper'

RSpec.describe "companies/edit", type: :view do
  # before(:each)は格it毎に実行される
  before(:each) do
    @company = assign(:company, FactoryBot.create(:company))
  end

  it "company編集フォームが描画されること" do
    render

    assert_select "form[action=?][method=?]", company_path(@company), "post" do

      assert_select "input[name=?]", "company[name]"
    end
  end
end
