require 'rails_helper'

RSpec.describe "companies/index", type: :view do
  before(:each) do
    assign(:companies, FactoryBot.create_list(:company, 2, name: 'テスト'))
  end

  it "company一覧が描画されること" do
    render
    assert_select "tr>td", text: "テスト".to_s, count: 2
  end
end
