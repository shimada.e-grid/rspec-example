require 'rails_helper'

RSpec.describe Employee, type: :model do
  describe 'アソシエーションテスト' do
    let(:association) {described_class.reflect_on_association(target)}
    context 'Companyモデル' do
      let(:target) {:company}
      it 'belongs_to関連付けされていること' do
        expect(association.macro).to eq(:belongs_to)
      end
      it 'モデル名がCompanyであること' do
        expect(association.class_name).to eq('Company')
      end
    end
  end

  describe 'バリデーションテスト' do
    context '有効な値の場合' do
      let (:employee) { FactoryBot.build(:employee) }
      it '有効であること' do
        expect(employee).to be_valid
      end
    end

    context 'nameカラム' do
      let (:employee) { FactoryBot.build(:employee, name: name) }
      context '空白の場合' do
        let(:name) { '' }
        it '無効であること' do
          expect(employee).not_to be_valid
        end
        it 'エラーメッセージを返すこと' do
          employee.valid?
          expect(employee.errors[:name]).to include(I18n.t('errors.messages.blank'))
        end
      end
      context '51文字以上の場合' do
        let(:name) { 'あ'*51 }
        it '無効であること' do
          expect(employee).not_to be_valid
        end
        it 'エラーメッセージを返すこと' do
          employee.valid?
          expect(employee.errors[:name]).to include(I18n.t('errors.messages.too_long', count: 50))
        end
      end
    end
  end
end
