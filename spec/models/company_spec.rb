require 'rails_helper'

RSpec.describe Company, type: :model do
  describe 'アソシエーションテスト' do
    let(:association) {described_class.reflect_on_association(target)}
    context 'Officeモデル' do
      let(:target) {:offices}
      it 'has_many関連付けされていること' do
        expect(association.macro).to eq(:has_many)
      end
      it 'Companyが削除されたら一緒に削除されること' do
        expect(association.options[:dependent]). to eq(:destroy)
      end
      it 'モデル名がCompanyであること' do
        expect(association.class_name).to eq('Office')
      end
    end
    context 'Employeeモデル' do
      let(:target) {:employees}
      it 'has_many関連付けされていること' do
        expect(association.macro).to eq(:has_many)
      end
      it 'Companyが削除されたら一緒に削除されること' do
        expect(association.options[:dependent]). to eq(:destroy)
      end
      it 'モデル名がCompanyであること' do
        expect(association.class_name).to eq('Employee')
      end
    end
    context 'Projectモデル' do
      let(:target) {:projects}
      it 'has_many関連付けされていること' do
        expect(association.macro).to eq(:has_many)
      end
      it 'Companyが削除されたら一緒に削除されること' do
        expect(association.options[:dependent]). to eq(:destroy)
      end
      it 'モデル名がCompanyであること' do
        expect(association.class_name).to eq('Project')
      end
    end
  end

  describe 'バリデーションテスト' do
    # 正常ケース
    context '有効な値の場合' do
      # letのcompanyが呼ばれると、FactoryBotがCompanyオブジェクトを作成する
      let (:company) { FactoryBot.build(:company) }
      it '有効であること' do
        # 有効かテスト
        # be_validを使い、company.valid?を省略
        # expect(company.valid?).to be_truthy
        expect(company).to be_valid
      end
    end

    # 異常ケース
    context 'nameカラム' do
      # nameキーの値は、後に定義されるletに置き換わる
      let (:company) { FactoryBot.build(:company, name: name) }
      context '空白の場合' do
        # nameの値を空白にする
        let(:name) { '' }
        it '無効であること' do
          # ここで呼ばれるletで定義したcompanyについて
          # 1. companyが呼ばれる -> FacotryBotのcompanyオブジェクトが作成される
          # 2. companyオブジェクトのnameキーの値(name)が呼ばれる -> 空白のnameが設定される
          # 3. 出来上がったcompanyの中身 -> {id: nil, name: ''}
          expect(company).not_to be_valid
        end
        it 'エラーメッセージを返すこと' do
          # 有効か判定（nameが空白なのでエラーを起こし、エラーメッセージが格納される）
          company.valid?
          # エラーメッセージの中身が想定のメッセージかテスト
          expect(company.errors[:name]).to include(I18n.t('errors.messages.blank'))
        end
      end
      context '101文字以上の場合' do
        # nameの値を101文字にする
        let(:name) { 'あ'*101 }
        it '無効であること' do
          expect(company).not_to be_valid
        end
        it 'エラーメッセージを返すこと' do
          company.valid?
          expect(company.errors[:name]).to include(I18n.t('errors.messages.too_long', count: 100))
        end
      end
    end
  end
end
