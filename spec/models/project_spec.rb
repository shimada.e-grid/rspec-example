require 'rails_helper'

# gemのshoulda-matchersを使用した場合
RSpec.describe Project, type: :model do
  describe 'アソシエーションテスト' do
    context 'Companyモデル' do
      it 'Companyモデルとbelongs_to関連付けされていること' do
        should belong_to(:company).class_name('Company')
      end
    end
    context 'Employeeモデル' do
      it 'Employeeモデルとhas_many関連付け、EmployeeProjectモデルとthrough関連付けされていること' do
        should have_many(:employees).through(:employee_projects).class_name('Employee')
      end
    end
    context 'EmployeeProjectモデル' do
      it 'EmployeeProjectモデルとhas_many関連付けされていること' do
        should have_many(:employee_projects).class_name('EmployeeProject')
      end
    end
  end

  describe 'モデルの親子関係テスト' do
    it 'EmployeeProjectモデル' do
      should accept_nested_attributes_for(:employee_projects).allow_destroy(true)
    end
  end

  describe 'バリデーションテスト' do
    context '有効な値の場合' do
      let (:project) { FactoryBot.build(:project) }
      it '有効であること' do
        expect(project).to be_valid
      end
    end

    context 'nameカラム' do
      subject { FactoryBot.create(:project) }
      it '必須入力が設定されること' do
        should validate_presence_of(:name)
      end
      it '最大文字数が100文字が設定されること' do
        should validate_length_of(:name).is_at_most(100)
      end
    end
  end

  describe 'インスタンスメソッド' do
    context '#employee_projects_or_build' do
      context 'プロジェクトに紐ずく従業員情報がDBに存在する場合' do
        context '全ての従業員が紐ずく場合' do
          it '紐付いたプロジェクトメンバーインスタンスを返すこと' do
            skip()
          end
        end
        context '一部の従業員が紐ずく場合' do
          it '一部の紐付いたプロジェクトメンバーインスタンスを返すこと' do
            skip()
          end
          it '紐付いてないプロジェクトメンバーは空のインスタンスを返すこと' do
            skip()
          end
        end
      end
      context 'プロジェクトに紐ずく従業員情報がDBに存在しない場合' do
        it '空のプロジェクトメンバーインスタンスを返すこと' do
          skip()
        end
      end
    end

    context '#employee_projects_or_build_by_attributes' do
      context 'プロジェクトに紐ずく従業員インスタンスが存在する場合' do
        it '紐付いたプロジェクトメンバーのis_checkedカラムオブジェクトがtrueになること' do
          skip()
        end
      end
      context '引数のプロジェクトメンバーの_destroyカラムオブジェクトが1の場合' do
        it '空のプロジェクトメンバーインスタンスが設定されること' do
          skip()
        end
      end
    end
  end
end
