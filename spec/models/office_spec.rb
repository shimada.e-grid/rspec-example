require 'rails_helper'

RSpec.describe Office, type: :model do
  describe 'アソシエーションテスト' do
    let(:association) {described_class.reflect_on_association(target)}
    context 'Employeeモデル' do
      let(:target) {:employees}
      it 'has_many関連付けされていること' do
        expect(association.macro).to eq(:has_many)
      end
      it 'Employeeが削除されても一緒に削除されないこと' do
        expect(association.options[:dependent]). to eq(:nullify)
      end
      it 'モデル名がEmployeeであること' do
        expect(association.class_name).to eq('Employee')
      end
    end
    context 'Companyモデル' do
      let(:target) {:company}
      it 'belongs_to関連付けされていること' do
        expect(association.macro).to eq(:belongs_to)
      end
      it 'モデル名がCompanyであること' do
        expect(association.class_name).to eq('Company')
      end
    end
  end

  describe 'バリデーションテスト' do
    context '有効な値の場合' do
      let (:office) { FactoryBot.build(:office) }
      it '有効であること' do
        expect(office).to be_valid
      end
    end

    context 'main_office_flagカラム' do
      context "#main_office_cannot_be_multiple_selection_when_create" do
        context '新規作成時、既に同じ企業に本社が存在していた場合' do
          it '無効であること' do
            skip()
          end
          it 'エラーメッセージを返すこと' do
            skip()
          end
        end
      end
      context '#main_office_cannot_be_multiple_selection_when_update' do
        context '更新時、既に同じ企業に本社が存在していた場合' do
          it '無効であること' do
            skip()
          end
          it 'エラーメッセージを返すこと' do
            skip()
          end
        end
      end
    end

    context 'nameカラム' do
      let (:office) { FactoryBot.build(:office, name: name) }
      context '空白の場合' do
        let(:name) { '' }
        it '無効であること' do
          expect(office).not_to be_valid
        end
        it 'エラーメッセージを返すこと' do
          office.valid?
          expect(office.errors[:name]).to include(I18n.t('errors.messages.blank'))
        end
      end
      context '101文字以上の場合' do
        let(:name) { 'あ'*101 }
        it '無効であること' do
          expect(office).not_to be_valid
        end
        it 'エラーメッセージを返すこと' do
          office.valid?
          expect(office.errors[:name]).to include(I18n.t('errors.messages.too_long', count: 100))
        end
      end
    end
  end
end
