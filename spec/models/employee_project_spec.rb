require 'rails_helper'

RSpec.describe EmployeeProject, type: :model do
  describe 'アソシエーションテスト' do
    context 'EmployeeProjectモデル' do
      it 'Employeeモデルとbelongs_to関連付けされていること' do
        should belong_to(:employee).class_name('Employee')
      end
      it 'Projectモデルとbelongs_to関連付けされていること' do
        should belong_to(:project).class_name('Project')
      end
    end
  end

  describe 'バリデーションテスト' do
    context '有効な値の場合' do
      let (:employee_project) { FactoryBot.build(:employee_project) }
      it '有効であること' do
        expect(employee_project).to be_valid
      end
    end

    context 'join_dateカラム' do
      let (:employee_project) { FactoryBot.build(:employee_project, join_date: join_date, leave_date: Date.today) }
      context 'leave_dateより以後の日付の場合' do
        let(:join_date) { Date.tomorrow }
        it '無効であること' do
          employee_project.valid?
          expect(employee_project.errors[:join_date]).to include(I18n.t('errors.messages.less_than_or_equal_to_date', attribute: I18n.t('activerecord.attributes.employee_project.leave_date')))
        end
      end
    end

    context 'leave_dateカラム' do
      let (:employee_project) { FactoryBot.build(:employee_project, join_date: Date.today, leave_date: leave_date) }
      context 'join_dateより以前の日付の場合' do
        let(:leave_date) { Date.yesterday }
        it '無効であること' do
          employee_project.valid?
          expect(employee_project.errors[:leave_date]).to include(I18n.t('errors.messages.greater_than_or_equal_to_date', attribute: I18n.t('activerecord.attributes.employee_project.join_date')))
        end
      end
    end
  end
end
