FactoryBot.define do
  factory :company do
    sequence(:name, "株式会社テスト_1")

    trait :with_all do
      after(:build) do |company|
        company.offices << FactoryBot.build(:office, company: company)
        company.offices << FactoryBot.build(:office, :branch_office, company: company)
        company.projects << FactoryBot.build(:project, company: company)
      end
    end
  end
end
