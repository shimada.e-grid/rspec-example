FactoryBot.define do
  factory :project do
    association :company
    name { "テストプロジェクト_1" }
  end
end
