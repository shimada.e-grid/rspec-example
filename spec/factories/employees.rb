FactoryBot.define do
  factory :employee do
    association :company
    association :office
    sequence(:number, "test_1")
    sequence(:name, "テスト太郎_1")
  end
end
