FactoryBot.define do
  factory :office do
    association :company
    main_office_flag { true }
    sequence(:name, "本社_1")
    sequence(:address, "テスト県テスト市_1")
    sequence(:tel, "1234567890_1")

    trait :branch_office do
      main_office_flag { false }
      sequence(:name, "テスト支社_1")
    end

    trait :with_employees do
      after(:build) do |office|
        office.employees << FactoryBot.build(:employee, company: office.company, office: office)
        office.employees << FactoryBot.build(:employee, company: office.company, office: nil)
      end
    end
  end
end
