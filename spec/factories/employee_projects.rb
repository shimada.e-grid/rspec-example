FactoryBot.define do
  factory :employee_project do
    association :employee
    association :project
    join_date { "2021-10-01" }
    leave_date { "2021-10-31" }
  end
end
