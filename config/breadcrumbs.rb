crumb :root do
  link t('home.index'), root_path
end

crumb :companies do
  link t('companies.index'), companies_path
  parent :root
end

crumb :company do |company|
  link "#{company.name}", company_path(company)
  parent :companies
end

crumb :new_company do |company|
  link t('companies.new'), new_company_path
  parent :companies
end

crumb :edit_company do |company|
  link "#{company.name}の#{t('edit')}", edit_company_path(company)
  parent :companies
end

crumb :offices do |company|
  link t('offices.index'), company_offices_path
  parent :company, company
end

crumb :office do |office|
  link "#{office.name}", company_office_path(office.company, office)
  parent :offices, office.company
end

crumb :new_office do |company|
  link t('offices.index'), new_company_office_path
  parent :offices, company
end

crumb :edit_office do |office|
  link "#{office.name}の#{t('edit')}", edit_company_office_path(office)
  parent :offices, office.company
end

crumb :employees do |company|
  link t('employees.index'), company_employees_path
  parent :company, company
end

crumb :employee do |employee|
  link "#{employee.name}", company_employee_path(employee.company, employee)
  parent :employees, employee.company
end

crumb :new_employee do |company|
  link t('employees.new'), new_company_employee_path
  parent :employees, company
end

crumb :edit_employee do |employee|
  link "#{employee.name}の#{t('edit')}", edit_company_employee_path(employee)
  parent :employees, employee.company
end

crumb :projects do |company|
  link t('projects.index'), company_projects_path
  parent :company, company
end

crumb :project do |project|
  link "#{project.name}", company_project_path(project.company, project)
  parent :projects, project.company
end

crumb :new_project do |company|
  link t('projects.new'), new_company_project_path
  parent :projects, company
end

crumb :edit_project do |project|
  link "#{project.name}の#{t('edit')}", edit_company_project_path(project)
  parent :projects, project.company
end
