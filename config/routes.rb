Rails.application.routes.draw do
  scope '/api', format: :json do
    scope '/v1' do
      resources :companies do
        resources :offices
        resources :employees
        resources :projects
      end
    end
  end
  defaults format: :html do
    root 'home#index'
    get 'home/index'
    get '*path', to: 'home#index'
  end
end
