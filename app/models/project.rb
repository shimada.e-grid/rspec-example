class Project < ApplicationRecord
  belongs_to :company
  has_many :employee_projects, dependent: :destroy
  has_many :employees, through: :employee_projects

  accepts_nested_attributes_for :employee_projects, allow_destroy: true

  validates :name, presence: true, length: { maximum: 100 }

  def employee_projects_or_build
    self.company.employees.each do |employee|
      employee_project = employee.employee_projects.find_by(project: self)
      if employee_project.present?
        self.employee_projects << employee_project
      else
        self.employee_projects.build(employee: employee)
      end
    end
    return self
  end

  def employee_projects_or_build_by_attributes(employee_projects_attributes)
    self.employee_projects.each do |employee_project|
      employee_project.is_checked = true
    end
    employee_projects_attributes.each do |i, employee_project|
      if employee_project[:_destroy] == '1'
        self.employee_projects.build(employee_id: employee_project[:employee_id])
      end
    end
    return self
  end
end
