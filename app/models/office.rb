class Office < ApplicationRecord
  belongs_to :company
  has_many :employees, dependent: :nullify

  validates :name, presence: true, length: { maximum: 100 }
  validates :address, length: { maximum: 100 }
  validates :tel, length: { maximum: 50 }

  validate :main_office_cannot_be_multiple_selection_when_create, on: :create
  validate :main_office_cannot_be_multiple_selection_when_update, on: :update

  private

  def main_office_cannot_be_multiple_selection_when_create
    if main_office_flag && company.offices.where(main_office_flag: true).count >= 1
      errors.add(:main_office_flag, I18n.t('errors.messages.taken'))
    end
  end

  def main_office_cannot_be_multiple_selection_when_update
    if main_office_flag && company.offices.where(main_office_flag: true).where.not(id: id).count >= 1
      errors.add(:main_office_flag, I18n.t('errors.messages.taken'))
    end
  end
end
