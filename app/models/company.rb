class Company < ApplicationRecord
  has_many :offices, dependent: :destroy
  has_many :employees, dependent: :destroy
  has_many :projects, dependent: :destroy

  validates :name, presence: true, length: { maximum: 100 }
end
