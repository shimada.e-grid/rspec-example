class EmployeeProject < ApplicationRecord
  belongs_to :employee
  belongs_to :project

  attr_writer :_destroy
  attribute :is_checked, :boolean, default: false

  validate :valid_date_range?

  private

  def valid_date_range?
    if join_date? && leave_date?
      if join_date.after?(leave_date)
        errors.add(:join_date, I18n.t('errors.messages.less_than_or_equal_to_date', attribute: I18n.t('activerecord.attributes.employee_project.leave_date')))
        errors.add(:leave_date, I18n.t('errors.messages.greater_than_or_equal_to_date', attribute: I18n.t('activerecord.attributes.employee_project.join_date')))
      end
    end
  end

end
