class Employee < ApplicationRecord
  belongs_to :company
  belongs_to :office, optional: true
  has_many :employee_projects, dependent: :destroy
  has_many :projects, through: :employee_projects

  validates :name, presence: true, length: { maximum: 50 }
end
