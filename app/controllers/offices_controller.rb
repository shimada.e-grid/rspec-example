class OfficesController < ApplicationController
  before_action :set_office, only: [:show, :edit, :update, :destroy]
  before_action :set_company, only: [:index, :new, :create]

  # GET /offices
  def index
    @offices = @company.offices
  end

  # GET /offices/1
  def show
  end

  # GET /offices/new
  def new
    @office = @company.offices.new
  end

  # GET /offices/1/edit
  def edit
  end

  # POST /offices
  def create
    @office = @company.offices.new(office_params)

    if @office.save
      redirect_to company_office_url(@office.company, @office), notice: t('notice.created')
    else
      render :new
    end
  end

  # PATCH/PUT /offices/1
  def update
    if @office.update(office_params)
      redirect_to company_office_url(@office.company, @office), notice: t('notice.updated')
    else
      render :edit
    end
  end

  # DELETE /offices/1
  def destroy
    @office.destroy
    redirect_to company_offices_url, notice: t('notice.destroyed')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_office
      @office = Company.find(params[:company_id]).offices.find(params[:id])
    end

    def set_company
      @company = Company.find(params[:company_id])
    end

    # Only allow a list of trusted parameters through.
    def office_params
      params.require(:office).permit(:main_office_flag, :name, :address, :tel)
    end
end
