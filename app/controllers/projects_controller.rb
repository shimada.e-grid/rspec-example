class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :set_company, only: [:index, :new, :create]

  # GET /projects
  def index
    @projects = @company.projects
  end

  # GET /projects/1
  def show
  end

  # GET /projects/new
  def new
    @project = @company.projects.new
    @project.employee_projects_or_build
  end

  # GET /projects/1/edit
  def edit
    @project.employee_projects_or_build
  end

  # POST /projects
  def create
    @project = @company.projects.new(project_params)
    if @project.save
      redirect_to company_project_url(@project.company, @project), notice: t('notice.created')
    else
      @project.employee_projects_or_build_by_attributes(project_params[:employee_projects_attributes])
      render :new
    end
  end

  # PATCH/PUT /projects/1
  def update
    if @project.update(project_params)
      redirect_to company_project_url(@project.company, @project), notice: t('notice.updated')
    else
      @project.employee_projects_or_build_by_attributes(project_params[:employee_projects_attributes])
      render :edit
    end
  end

  # DELETE /projects/1
  def destroy
    @project.destroy
    redirect_to company_projects_url, notice: t('notice.destroyed')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Company.find(params[:company_id]).projects.find(params[:id])
    end

    def set_company
      @company = Company.find(params[:company_id])
    end

    # Only allow a list of trusted parameters through.
    def project_params
      params.require(:project).permit(:company_id, :name, employee_projects_attributes: [:id, :_destroy, :project_id, :employee_id, :join_date, :leave_date])
    end
end
