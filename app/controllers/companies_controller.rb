class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  # GET /companies
  def index
    @companies = Company.all
    render json: @companies, status: :ok
  end

  # GET /companies/1
  def show
    render json: @company, status: :ok
  end

  # GET /companies/new
  def new
    @company = Company.new
    render json: @company, status: :ok
  end

  # GET /companies/1/edit
  def edit
    render json: @company, status: :ok
  end

  # POST /companies
  def create
    @company = Company.new(company_params)

    if @company.save
      render json: @company, status: :created
      # redirect_to @company, notice: t('notice.created')
    else
      render json: { messages: @company.errors.messages }, status: :unprocessable_entity
      # render :new
    end
  end

  # PATCH/PUT /companies/1
  def update
    if @company.update(company_params)
      render json: @company, status: :ok
      # redirect_to @company, notice: t('notice.updated')
    else
      messages = @company.errors.map {|error| [error.attribute.to_s, error.full_message] }.to_h
      render json: { messages: messages }, status: :unprocessable_entity
      # render :edit
    end
  end

  # DELETE /companies/1
  def destroy
    if @company.destroy
      render json: @company, status: :ok
    else
      render json: { messages: @company.errors.messages }, status: :unprocessable_entity
    end
    # redirect_to companies_url, notice: t('notice.destroyed')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def company_params
      params.require(:company).permit(:name)
    end
end
