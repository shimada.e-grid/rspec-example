class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :edit, :update, :destroy]
  before_action :set_company, only: [:index, :new, :create]

  # GET /employees
  def index
    @employees = @company.employees
  end

  # GET /employees/1
  def show
  end

  # GET /employees/new
  def new
    @employee = @company.employees.new
  end

  # GET /employees/1/edit
  def edit
  end

  # POST /employees
  def create
    @employee = @company.employees.new(employee_params)

    if @employee.save
      redirect_to company_employee_url(@employee.company, @employee), notice: t('notice.created')
    else
      render :new
    end
  end

  # PATCH/PUT /employees/1
  def update
    if @employee.update(employee_params)
      redirect_to company_employee_url(@employee.company, @employee), notice: t('notice.updated')
    else
      render :edit
    end
  end

  # DELETE /employees/1
  def destroy
    @employee.destroy
    redirect_to company_employees_url, notice: t('notice.destroyed')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Company.find(params[:company_id]).employees.find(params[:id])
    end

    def set_company
      @company = Company.find(params[:company_id])
    end

    # Only allow a list of trusted parameters through.
    def employee_params
      params.require(:employee).permit(:office_id, :number, :name)
    end
end
