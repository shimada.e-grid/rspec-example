import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import { css } from '@emotion/react';

import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom';

import HeaderBar from './components/HeaderBar';
import Breadcrumbs from './components/Breadcrumbs';

import { default as Home } from './pages/home/index';
import * as Companies from './pages/companies/index.js';

const paperStyle = css`
  margin: 0rem;
  padding: 0rem;
`;

export default function App() {
  return (
    <>
      <CssBaseline />
      <HeaderBar />
      <main>
        <Container>
          <BrowserRouter>
            <Breadcrumbs />
            <Paper classes={paperStyle}>
              <Switch>
                <Route exact path='/' component={Home}></Route>
                <Route exact path='/home' component={Home}></Route>
                <Route exact path='/companies' component={Companies.Index}></Route>
                <Route exact path='/companies/new' component={Companies.New}></Route>
                <Route exact path='/companies/:company_id' component={Companies.Show}></Route>
                <Route exact path='/companies/:company_id/edit' component={Companies.Edit}></Route>
                <Route>404</Route>
              </Switch>
            </Paper>
          </BrowserRouter>
        </Container>
      </main>
    </>
  );
}
