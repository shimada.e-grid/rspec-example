import React, { useState } from 'react';
import { useForm } from "react-hook-form";
import { useHistory } from 'react-router-dom';

export default function From(props) {
  if (!props.company) {
    return null;
  }
  const { register, handleSubmit, reset } = useForm({
    defaultValues: props.company
  });
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const history= useHistory();

  const onSubmit = data => {
    fetch(props.url, {
      method: props.method,
      body: JSON.stringify({ company: data }),
      headers: { "Content-type": "application/json; charset=UTF-8" },
    })
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          if (result.messages) {
            console.log(result.messages)
          } else {
            history.push(`/companies/${result.id}`);
          }
        },
        (e) => {
          console.log('error', e)
          setIsLoaded(true);
          setError(e);
        }
      )
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input {...register("name")} />
      <input type="submit" />
    </form>
  );
}
