import React from 'react';
import MuiBreadcrumbs from '@mui/material/Breadcrumbs';
import {
  Link,
  Route,
  useRouteMatch,
} from 'react-router-dom';

export default function Breadcrumbs() {
  const companiesIndex = useRouteMatch("/companies");
  const companiesNew = useRouteMatch("/companies/new");
  const companiesShow = useRouteMatch("/companies/:company_id(\\d+)");
  const companiesEdit = useRouteMatch("/companies/:company_id(\\d+)/edit");
  return (
    <>
      <Route>
        <MuiBreadcrumbs aria-label="Breadcrumb">
          <Link color="inherit" to="/">
            Home
          </Link>
          {companiesIndex && (
            < Link color="inherit" to="/companies">
              一覧
            </Link>
          )}
          {companiesNew && (
            < Link color="inherit" to="/companies/new">
              新規追加
            </Link>
          )}
          {companiesShow && (
            < Link color="inherit" to={`/companies/${companiesShow.params.company_id}`}>
              詳細
            </Link>
          )}
          {companiesEdit && (
            < Link color="inherit" to={`/companies/${companiesEdit.params.company_id}/edit`}>
              編集
            </Link>
          )}
        </MuiBreadcrumbs>
      </Route>
    </>
  );
}
