import React, { useState } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useHistory } from 'react-router-dom';

export default function DestroyButton(props) {
  if (!props.url) {
    return null;
  }
  const [open, setOpen] = useState(false);
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const history= useHistory();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickDestroy = () => {
    fetch(props.url, {
      method: 'DELETE',
    })
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setOpen(false);
          history.go(0);
        },
        (error) => {
          console.log(error)
          setIsLoaded(true);
          setError(error);
          setOpen(false);
        }
      )
  };

  return (
    <div>
      <Button size="small" onClick={handleClickOpen}>
        削除
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          本当に削除しますか？
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            削除した情報は元に戻りません。
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>キャンセル</Button>
          <Button onClick={handleClickDestroy} autoFocus>削除</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
