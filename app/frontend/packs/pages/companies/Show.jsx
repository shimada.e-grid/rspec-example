import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";

export default function Show() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [company, setCompany] = useState([]);
  const { company_id } = useParams();

  useEffect(() => {
    fetch(`/api/v1/companies/${company_id}`)
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setCompany(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, []);

  return (
    <>
      { company.name }
    </>
  );
}
