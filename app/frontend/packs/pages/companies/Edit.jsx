import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import Form from '../../components/company/Form';

export default function Edit() {
  const [company, setCompany] = useState({});
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const { company_id } = useParams();

  useEffect(() => {
    fetch(`/api/v1/companies/${company_id}`)
      .then(res => res.json())
      .then(
        (result) => {
          setCompany(result);
          setIsLoaded(true);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, []);

  return (
    <>
      {isLoaded && <Form company={company} url={`/api/v1/companies/${company_id}`} method={'PATCH'} />}
    </>
  );
}
