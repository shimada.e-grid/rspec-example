import React, { useState, useEffect } from 'react';
import Form from '../../components/company/Form';

export default function New() {
  const [company, setCompany] = useState({});
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    fetch(`/api/v1/companies/new`)
      .then(res => res.json())
      .then(
        (result) => {
          setCompany(result);
          setIsLoaded(true);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, []);

  return (
    <>
      {isLoaded && <Form company={company} url={`/api/v1/companies`} method={'POST'} />}
    </>
  );
}
