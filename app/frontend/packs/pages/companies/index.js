export { default as Index } from './Index.jsx'
export { default as Edit } from './Edit';
export { default as New } from './New';
export { default as Show } from './Show';