# マルチステージビルドの利用
FROM node:14.17-slim as node

FROM ruby:2.7.4-slim

ENV LANG ja_JP.UTF-8 \
  TZ Asia/Tokyo

# 作業ディレクトリを指定
WORKDIR /home/app/rspec-example

# --no-install-recommends 必須パッケージのみインストール
# apt-get clean キャッシュ削除
# rm -rf /var/lib/apt/lists/* キャッシュ削除
RUN apt-get update -qq \
  && apt-get install -y --no-install-recommends \
  build-essential \
  postgresql-client \
  libpq-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# マルチステージビルドからコピー
COPY --from=node /usr/local/bin/node /usr/local/bin/node
COPY --from=node /opt/yarn-* /opt/yarn
RUN ln -fs /opt/yarn/bin/yarn /usr/local/bin/yarn

# bundle installに必要なファイルをコピー
COPY Gemfile Gemfile.lock ./
RUN bundle config set path "vendor/bundle" \
  && bundle install --jobs=3 --retry=3

# yarn installに必要なファイルをコピー
COPY package.json yarn.lock ./
RUN yarn install

# 全てコピー
COPY . ./

# entrypointで実行したいファイルをコピー
COPY ./entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
# コンテナ実行時に必ず実行したいコマンド
ENTRYPOINT ["entrypoint.sh"]

# 実際にはポートを公開しません
EXPOSE 3000
# コンテナ実行時、デフォルトで実行したいコマンド（引数で上書き可能）
CMD ["bundle", "exec", "rails", "-p", "3000", "-b" , "0.0.0.0"]